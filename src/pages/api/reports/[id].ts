import { closeReport } from "../../../repositories/reports";
import type { NextApiRequest, NextApiResponse } from "next";
import { closeReportSchema } from "../../../interfaces/report";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<void>
) {
  await sleep(1500 * Math.random()); // for demo purposes

  const reportId = req.query.id;
  if (typeof reportId !== "string")
    throw new Error("impossible scenario, used for type narrowing");

  const parsedEvent = closeReportSchema.parse(req.body);

  if (parsedEvent.reason === "BLOCKED") {
    // here you could handle anything else involved in a block e.g. suspending a user
    closeReport(reportId);
  } else if (parsedEvent.reason === "RESOLVED") {
    closeReport(reportId);
  }
  res.status(200).json();
}

async function sleep(ms: number) {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
}
