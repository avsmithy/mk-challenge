import { listAllReports } from "../../../repositories/reports";
import type { NextApiRequest, NextApiResponse } from "next";
import { IReport } from "../../../interfaces/report";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<IReport[]>
) {
  const reports = listAllReports();
  res.status(200).json(reports);
}
