
import { LoadingElement, Layout } from '../frontend/components'
import { ConnectedReport, useReports } from '../frontend/report'
import { Text } from "@chakra-ui/react"

function ReportsList() {
    const { data, error, isLoading } = useReports()

    if (error) {
        return (
            <Layout title="Reports">
                <Text>There was an error loading reports</Text>
            </Layout>
        )
    }

    if (isLoading || !data) {
        return (
            <Layout title="Reports">
                <LoadingElement />
            </Layout>
        )
    }

    return (
        <Layout title="Reports">
            {data.map(report => <ConnectedReport key={report.id} {...report} />)}
        </Layout>
    )
}

export default ReportsList