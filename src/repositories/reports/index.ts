import { join, dirname } from "node:path";
import { fileURLToPath } from "node:url";
import { JSONFileSync, LowSync } from "lowdb";
import { IReport } from "../../interfaces/report";
import { InvalidTransitionError } from "../../interfaces/errors";

/**
 * This a simple mock of the storage layer for development purposes.
 */

interface RawReport {
  id: string;
  state: "OPEN" | "CLOSED";
  payload: {
    source: "REPORT";
    reportType: "SPAM";
    message: string | null;
  };
  created: string;
}

const __dirname = dirname(fileURLToPath(import.meta.url));
const file = join(__dirname, "../../../db/reports.json");

const db = new LowSync(new JSONFileSync<{ elements: RawReport[] }>(file));
db.read();

if (db.data == null) {
  throw new Error('db is empty - try running "yarn reset-db"');
}

export function listAllReports(): IReport[] {
  if (!db.data) throw new Error("db not initialised");

  return db.data.elements
    .map((report) => ({
      id: report.id,
      created: report.created,
      state: report.state,
      reportType: report.payload.reportType,
      message: report.payload.message ?? "",
    }))
    .filter((r) => r.state === "OPEN");
}

export function closeReport(id: string) {
  if (!db.data) throw new Error("db not initialised");

  db.data.elements = db.data.elements.map((r) => {
    if (r.id === id) {
      if (r.state !== "OPEN") {
        throw new InvalidTransitionError("report already closed");
      }
      return {
        ...r,
        state: "CLOSED",
      };
    }
    return r;
  });

  return db.write();
}
