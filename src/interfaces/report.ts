import z from "zod";

export const reportSchema = z
  .object({
    id: z.string().min(10),
    created: z.string().min(10),
    state: z.enum(["OPEN", "CLOSED"]),
    reportType: z.enum(["SPAM", "VIOLATES_POLICIES", "INFRINGES_PROPERTY"]),
    message: z.string(),
  })
  .required();

export type IReport = z.infer<typeof reportSchema>;

export const closeReportSchema = z
  .object({
    ticketState: z.enum(["CLOSED"]),
    reason: z.enum(["RESOLVED", "BLOCKED"]),
  })
  .required();
