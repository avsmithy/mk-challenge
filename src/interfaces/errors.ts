export class InvalidTransitionError extends Error {
  constructor(message: string) {
    super(message);
  }
}
