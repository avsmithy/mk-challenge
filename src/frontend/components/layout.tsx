import { Container, Box, Heading } from "@chakra-ui/react"
import { ReactNode } from "react"

export function Layout({ title, children }: { title: string, children: ReactNode }) {
    return (
        <Box role="main" as="main" height="100vh" overflowY="auto">
            <Container pt={{ base: '4', lg: '12' }} pb={{ base: '8', lg: '24' }}>
                <Heading as='h1'>{title}</Heading>
                {children}
            </Container>
        </Box>
    )
}