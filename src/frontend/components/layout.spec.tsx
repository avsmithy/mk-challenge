import { expect, test } from 'vitest'
import { render, screen, within } from '@testing-library/react'
import { Layout } from './layout'

test('layout', () => {
    render(<Layout title="test title">hello world</Layout>)
    const main = within(screen.getByRole('main'))
    expect(main.getByRole('heading', { level: 1, name: /test title/i })).toBeDefined()
    expect(document.body).toMatchSnapshot()
})
