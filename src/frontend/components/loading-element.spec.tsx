import { expect, test } from 'vitest'
import { render, screen } from '@testing-library/react'
import { LoadingElement } from './loading-element'

test('loading element', async () => {
    render(<LoadingElement />)
    const el = await screen.findByText(/loading/i)
    expect(el).toMatchSnapshot()
})
