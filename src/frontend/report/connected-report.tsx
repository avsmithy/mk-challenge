import { Button, Spacer } from "@chakra-ui/react"
import { IReport } from '../../interfaces/report'
import { useBlockReport, useResolveReport } from "./api"
import { Report } from "./report"

export function ConnectedReport(report: IReport) {
  const block = useBlockReport(report.id)
  const resolve = useResolveReport(report.id)

  const Block = (
    <Button
      colorScheme='blue'
      variant="solid"
      onClick={block.trigger}
      isLoading={block.isMutating}>
      Block
    </Button>
  )

  const Resolve = (
    <Button
      colorScheme='blue'
      variant="solid"
      onClick={resolve.trigger}
      isLoading={resolve.isMutating}>
      Resolve
    </Button>
  )

  return (
    <Report {...report} hasError={block.error || resolve.error}>
      {Block}
      <Spacer />
      {Resolve}
    </Report>)
}
