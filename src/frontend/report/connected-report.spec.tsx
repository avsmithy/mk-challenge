import { test, vi, expect, afterEach } from 'vitest'
import { render, screen, waitFor } from '@testing-library/react'
import { ConnectedReport } from './connected-report'
import { useBlockReport } from './api'

vi.mock('./api', () => {
  const blockTriggerMock = vi.fn()
  return {
    useResolveReport: vi.fn(() => ({
      isMutating: false,
      error: false,
      trigger: vi.fn()
    })),
    useBlockReport: vi.fn(() => ({
      isMutating: false,
      error: false,
      trigger: blockTriggerMock
    }))
  }
})

afterEach(() => {
  vi.clearAllMocks()
})

test('connected report: block button triggers mutation', async () => {
  render(<ConnectedReport
    id="123"
    created="sometime"
    state="OPEN"
    message="my message"
    reportType="INFRINGES_PROPERTY" />)

  expect(useBlockReport).toHaveBeenCalledOnce()
  expect(useBlockReport).toHaveBeenCalledWith('123')

  expect(useBlockReport('').trigger).not.toHaveBeenCalled()
  screen.getByText(/block/i).click()

  await waitFor(() => {
    expect(useBlockReport('').trigger).toHaveBeenCalledOnce()
  })
})