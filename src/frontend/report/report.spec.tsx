import { beforeEach, expect, test, describe } from 'vitest'
import { cleanup, render, screen } from '@testing-library/react'
import { Report } from './report'

beforeEach(() => {
    cleanup()
})

describe('Report', () => {
    test('snapshot', () => {
        render(
            <Report
                id="123"
                created="sometime"
                state="OPEN"
                message="my message"
                reportType="INFRINGES_PROPERTY">
                <div>hello</div><div>hello</div>
            </Report>)
        expect(document.body).toMatchSnapshot()
    })

    test('check content is rendered', async () => {
        render(
            <Report
                id="456"
                created="sometime"
                state="OPEN"
                message="my message"
                reportType="INFRINGES_PROPERTY">
                <div>hello</div><div>hello</div>
            </Report>)
        expect(screen.getByText(/456/i)).toBeDefined()
        expect(screen.getByText(/open/i)).toBeDefined()
        expect(screen.getByText(/my message/i)).toBeDefined()
        expect(screen.getByText(/infringes property/i)).toBeDefined()
        expect(await screen.findAllByText(/hello/i)).toHaveLength(2)
    })

    test('error message is visible when hasError=true', () => {
        render(
            <Report
                hasError={true}
                id="456"
                created="sometime"
                state="OPEN"
                message="my message"
                reportType="INFRINGES_PROPERTY">
                <div>hello</div><div>hello2</div>
            </Report>)
        expect(screen.getByText(/request failed/i)).toBeDefined()
    })
})