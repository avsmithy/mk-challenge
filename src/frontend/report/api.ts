import ky from "ky";
import useSWR from "swr";
import useSWRMutation from "swr/mutation";
import { IReport } from "../../interfaces/report";

// Note:
// If desired for UX, you could implement optimistic updates with rollback on error with SWR:
// https://swr.vercel.app/docs/mutation

async function fetcher<T>(url: string): Promise<T> {
  return await ky.get("/api/reports").json<T>();
}

const blockReport = (id: string) => async () => {
  return await ky
    .put(`/api/reports/${id}`, {
      json: {
        // for this example, change the CLOSED string to anything
        // and then click Block to see zod in action
        ticketState: "CLOSED",
        reason: "BLOCKED",
      },
    })
    .json();
};

const resolveReport = (id: string) => async () => {
  await ky
    .put(`/api/reports/${id}`, {
      json: {
        ticketState: "CLOSED",
        reason: "RESOLVED",
      },
    })
    .json();
};

export const useReports = () => useSWR<IReport[]>("/api/reports", fetcher);

export const useResolveReport = (id: string) =>
  useSWRMutation(`/api/reports`, resolveReport(id));

export const useBlockReport = (id: string) =>
  useSWRMutation(`/api/reports`, blockReport(id));
