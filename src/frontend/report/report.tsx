import { Container, Stack, Box, Text, HStack, VStack, Link, Flex, Spacer } from "@chakra-ui/react"
import { ReactNode } from "react"
import { IReport } from '../../interfaces/report'

const ReportTypeToText: Record<IReport["reportType"], string> = {
  'SPAM': 'Spam',
  'VIOLATES_POLICIES': 'Violates Policies',
  'INFRINGES_PROPERTY': 'Infringes Property'
}

const StateToText: Record<IReport["state"], string> = {
  'OPEN': 'Open',
  'CLOSED': 'Closed'
}

export interface ReportProps extends IReport {
  children: ReactNode[]
  hasError?: boolean
}

export function Report({ id, state, reportType, message, children, hasError }: ReportProps) {
  return (
    <Box as="section" py={{ base: '2', md: '4' }}>
      <Container maxW="3xl">
        <Box bg={hasError ? "red.100" : "gray.100"} boxShadow="sm" borderRadius="lg" p={{ base: '4', md: '6' }}>
          <Flex>
            <VStack w="32" flexShrink="0" align={'start'} >
              <Text wordBreak='break-all' noOfLines={1} w="full">ID: {id}</Text>
              <Text>State: {StateToText[state]}</Text>
              <Link href="#" color="teal">Details</Link>
            </VStack>
            <VStack flexGrow="1" align={'start'} ml="4">
              <Text>Type: {ReportTypeToText[reportType]}</Text>
              <Text>Message: {message}</Text>
            </VStack>
            <VStack w="32" flexShrink="0" align='end'>
              {children}
              {hasError && (
                <Text align='right' color="red.900">Request Failed</Text>
              )}
            </VStack>
          </Flex>
        </Box>
      </Container>
    </Box>
  )
}
