# MK Challenge

## Getting Started

1. Run `yarn` to install dependencies.
1. Run `yarn reset-db` to make a copy of the json db file.
1. Then run `yarn dev` to start the dev server.
1. Then run `yarn test` to run tests.

Note, if you do not have yarn available, you may be able to directly refer to the cached binary e.g.:

`./.yarn/releases/yarn-3.4.1.cjs`

### Development

Currently configured for VS Code only.

You must select the workspace version of TS for Yarn PnP to work in the IDE: [manual](https://code.visualstudio.com/docs/typescript/typescript-compiling#_using-the-workspace-version-of-typescript)

## Testing

There are some unit tests as an example. For a real application there would be higher coverage of the different components, api's and permuations.

Integration and E2E tests would follow.

## Technology choices

### Yarn with PnP

It is fast, performant and allows dependencies to be checked in due to its zipped filesystem, improving CI performance and removing external dependencies when building or checking out a project.

### Next.js

Incorporates a frontend and backend layer, ideal for this small project. Popular and well supported by tooling and the community. Would consider a more established backend framework like Nest if the application was expected to expand.

### SWR

Automatically caches and refreshes data. Similar to react-query.

### Zod

Excellent for defining an API schema to be used in both frontend and the API. Allows validation of message bodies and TypeScript type inference from the same schema.

### Chakra UI

Opinionated react UI framework built on top of Tailwind CSS.

### TypeScript

Industry standard.

### LowDB

Simple db layer for the mock json file.
