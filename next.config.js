module.exports = {
    async redirects() {
      return [
        {
          source: '/',
          destination: '/reports',
          permanent: false,
        },
      ]
    },
  }